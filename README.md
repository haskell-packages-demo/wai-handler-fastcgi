# wai-handler-fastcgi

https://www.stackage.org/package/wai-handler-fastcgi

## Stability status
* https://tracker.debian.org/pkg/haskell-wai-handler-fastcgi

## Official documentation
* [*Web Application Interface*
  ](https://www.yesodweb.com/book/web-application-interface)